module daemonsauce

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/miekg/dns v1.1.31
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
)

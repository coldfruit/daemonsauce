# Deploying

The single executable `daemonsauced` contains everything needed to run the application.

Most configuration can be done at runtime using the web ui, however there are some configuration settings which need to 
be set correctly at startup.  You can use the following environment variables for these.

## Environment variables
```
$HTTP_PORT  = 8080      # the port to run the web ui and REST API on
$TSIG       =           # optional TSIG configuration for the DNS server   
```

# Development Prerequisites

You will need npm, nodejs, `quasar` (`npm install -g @quasar/cli`), Go, and `esc` (`go get -u github.com/mjibson/esc`).

# Building

Start in the `webui` directory.  Run `quasar build`.

Next, come back up and run `go generate` followed by `go build`.

Finally, run the executable.  The web UI is embedded (so no live reload, sorry).

If you are only working on the frontend, you can use `quasar dev`, but bear in mind that you'll need to run the API at 
the same time and adjust the axios config accordingly (or write mocks for the API if I haven't already got around to 
it).
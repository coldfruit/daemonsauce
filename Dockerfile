FROM node:10.21.0 AS frontend

RUN npm install -g @quasar/cli

COPY webui /webui
WORKDIR /webui
RUN quasar build

FROM golang:1.15.0 AS backend

RUN go get -u github.com/mjibson/esc

COPY . /src
COPY --from=frontend /webui/dist/spa webui/dist/spa/
WORKDIR /src

RUN go generate
RUN go build -o daemonsauce

FROM scratch AS final

COPY --from=backend /src/daemonsauce /daemonsauce

CMD /daemonsauce
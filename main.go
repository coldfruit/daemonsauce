package main

import (
	"bytes"
	"daemonsauce/dsd"
	"fmt"
	"github.com/miekg/dns"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func serve(net, name, secret string, soreuseport bool) {
	switch name {
	case "":
		server := &dns.Server{Addr: "[::]:8053", Net: net, TsigSecret: nil, ReusePort: soreuseport}
		if err := server.ListenAndServe(); err != nil {
			fmt.Printf("Failed to setup the "+net+" server: %s\n", err.Error())

		}
	default:
		server := &dns.Server{Addr: ":8053", Net: net, TsigSecret: map[string]string{name: secret}, ReusePort: soreuseport}
		if err := server.ListenAndServe(); err != nil {
			fmt.Printf("Failed to setup the "+net+" server: %s\n", err.Error())
		}
	}
}

/**
DaemonSauceD DNS server.  Answers DNS queries.
*/
//go:generate esc -o dsd/client-assets.go -pkg dsd -prefix=webui/dist/spa webui/dist/spa
func main() {
	var name, secret, dataDir string
	tsigEnv, tsigSet := os.LookupEnv("TSIG")
	if tsigSet {
		tsig := strings.SplitN(tsigEnv, ":", 2)
		name, secret = dns.Fqdn(tsig[0]), tsig[1]
	} else {
		logrus.Warnf("No $TSIG variable set, running without TSIG support")
	}

	dataDirEnv, dataDirSet := os.LookupEnv("DATA_DIR")
	if dataDirSet {
		// remove any possible trailing slash
		dataDir = strings.TrimRight(dataDirEnv, "/")
	} else {
		dataDir = dsd.DefaultDataDir
	}

	zoneFiles, err := ioutil.ReadDir(dataDir)
	if err != nil {
		logrus.Errorf("Could not read from data directory: %v", err)
		os.Exit(dsd.FatalDataDirUnavailable)
	}

	for _, zf := range zoneFiles {
		// grab the base name of the file, and use that as the origin
		baseName := strings.Split(zf.Name(), ".zone")[0]
		fullPath := fmt.Sprintf("%s/%s", dataDir, zf.Name())
		fileContents, err := ioutil.ReadFile(fullPath)
		if err != nil {
			logrus.Warnf("Could not read file %s: %v", zf.Name(), err)
			continue
		}
		logrus.Infof("Parsing Zone %s", baseName)
		zp := dns.NewZoneParser(bytes.NewReader(fileContents), baseName, fullPath)
		for {
			rr, ok := zp.Next()
			if !ok {
				break
			}
			dns.HandleFunc(dns.Fqdn(baseName), func(writer dns.ResponseWriter, msg *dns.Msg) {
				response := new(dns.Msg)
				response.SetReply(msg)

				name := dns.Name(msg.Question[0].Name).String()
				if rr.Header().Name == name {
					response.Answer = append(response.Answer, rr)
				}

				err = writer.WriteMsg(response)
				if err != nil {
					logrus.Errorf("Could not write DNS response: %v", err)
				}
			})
		}

		logrus.Infof("Parsed Zone %s", baseName)
	}

	go serve("tcp", name, secret, true)
	go serve("udp", name, secret, true)

	go dsd.StartWebServer()

	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	s := <-sig
	logrus.Infof("Signal %s received, stopping", s)
}

package dsd

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"math/rand"
	"net/http"
)

func ApiServer() http.Handler {
	sm := mux.NewRouter().PathPrefix("/api").Subrouter()

	sm.HandleFunc("/stats", func(writer http.ResponseWriter, request *http.Request) {
		var err error
		writer.WriteHeader(200)
		writer.Header().Set("Content-Type", "application/json")
		// todo: replace with actual stats
		err = json.NewEncoder(writer).Encode(Stats{RequestsPerSecond: rand.Float64()})
		if err != nil {
			logrus.Errorf("Could not encode stats json: %v", err)
		}
	})

	return sm
}

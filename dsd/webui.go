package dsd

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

const defaultListenPort = "8080"

func StartWebServer() {
	var listenPort uint16
	webServer := http.NewServeMux()

	webServer.Handle("/api/", ApiServer())

	webServer.Handle("/", http.FileServer(FS(false)))

	listenPortStr, listenPortSet := os.LookupEnv("HTTP_PORT")
	if !listenPortSet {
		listenPortStr = defaultListenPort
	}
	listenPort64, err := strconv.ParseInt(listenPortStr, 10, 16)
	listenPort = uint16(listenPort64)

	logrus.Infof("Server listening on port %d", listenPort)

	err = http.ListenAndServe(fmt.Sprintf(":%d", listenPort), webServer)
	if err != nil {
		logrus.Errorf("Server crash detected: %v", err)
	}
	logrus.Infof("Web server shut down")
}
